package repositories

import (
	"api/models"
	"database/sql"
)

const insertUser = `
		INSERT INTO users (name, email, birth_date, document, password)
		VALUES ($1, $2, $3, $4, $5);`
const getUserByEmail = `SELECT * FROM users WHERE email = $1;`

func NewUserRepository(db *sql.DB) UserRepository {
	return UserRepository{Database: db}
}

type UserRepository struct {
	Database *sql.DB
}

func (ur UserRepository) Insert(user models.User) (*models.InsertResult, error) {
	var result models.InsertResult
	err := ur.Database.QueryRow(insertUser, user.Email, user.Name, user.BirthDate, user.Document, user.Password).Scan(&result.ID)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (ur *UserRepository) GetUserByEmail(email string) (*models.User, error) {
	var result models.User
	if err := ur.Database.QueryRow(getUserByEmail, email).Scan(&result.ID, &result.Email, &result.Password); err != nil {
		return nil, err
	}
	return &result, nil
}
