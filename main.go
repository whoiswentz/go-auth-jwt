package main

import (
	"api/app"
	"context"
	"github.com/joho/godotenv"
	"log"
	"os"
	"os/signal"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatalln(err)
	}
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	termChan := make(chan os.Signal, 1)
	signal.Notify(termChan, os.Interrupt)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-termChan
		cancel()
	}()

	app.StartApplication(ctx)
}
