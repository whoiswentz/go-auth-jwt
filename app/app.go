package app

import (
	"api/config"
	"api/controllers"
	"api/database"
	"api/middlewares"
	"context"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	readHeaderTimeout = 1 * time.Second
	writeTimeout      = 10 * time.Second
	idleTimeout       = 90 * time.Second
	maxHeaderBytes    = http.DefaultMaxHeaderBytes
)

func StartApplication(ctx context.Context) {
	addr := os.Getenv("PORT")

	db := database.NewConnection()
	defer database.CLoseConnection(db)

	uc := GenerateUserController(db)

	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler()).Methods(http.MethodGet)

	router.HandleFunc("/sign_up", uc.SignUp).Methods(http.MethodPost)
	router.HandleFunc("/login", uc.SignIn).Methods(http.MethodPost)

	router.HandleFunc("/protected", middlewares.TokenVerifyMiddleware(controllers.Protected)).Methods(http.MethodGet)

	server := &http.Server{
		Addr:              addr,
		Handler:           router,
		TLSConfig:         config.NewTlsConfig(),
		ReadHeaderTimeout: readHeaderTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		MaxHeaderBytes:    maxHeaderBytes,
	}

	go startServer(server)

	<-ctx.Done()

	shutdown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(shutdown); err != nil {
		log.Fatalf("Server shutdown failed: %s\n", err)
	}

	log.Println("Server exited properly")
}

func startServer(server *http.Server) {
	log.Printf("Server running on port %s\n", server.Addr)
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Could not listen on %s: %v\n", server.Addr, err)
	}
}
