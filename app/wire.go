//+build wireinject

package app

import (
	"api/controllers"
	"api/repositories"
	"api/services"
	"api/utils"
	"database/sql"
	"github.com/google/wire"
)

func GenerateUserController(db *sql.DB) controllers.UserController {
	panic(wire.Build(
		controllers.NewUserController,
		services.NewUserService,
		utils.NewRequestMetrics,
		utils.NewMetrics,
		repositories.NewUserRepository,
	))
	return controllers.UserController{}
}