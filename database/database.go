package database

import (
	"database/sql"
	"github.com/lib/pq"
	"log"
	"os"
)

func NewConnection() *sql.DB {
	url, err := pq.ParseURL(os.Getenv("DB_URL"))
	if err != nil {
		log.Fatalln(err)
	}

	db, err := sql.Open("postgres", url)
	if err != nil {
		log.Fatalln(err)
	}

	return db
}

func CLoseConnection(db *sql.DB) {
	log.Println("Closing the DB connection.")
	if err := db.Close(); err != nil {
		log.Fatalln(err)
	}
	log.Println("DB connection closed.")
}