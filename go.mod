module api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/mock v1.4.3 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/wire v0.4.0
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.3.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
