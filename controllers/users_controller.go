package controllers

import (
	"api/models"
	"api/services"
	"encoding/json"
	"log"
	"net/http"
)

func NewUserController(s services.UserService) UserController {
	return UserController{UserService: s}
}

type UserController struct {
	UserService services.UserService
}

func (us *UserController) SignUp(writer http.ResponseWriter, request *http.Request) {
	var user models.User
	if err := json.NewDecoder(request.Body).Decode(&user); err != nil {
		log.Fatalln(err)
	}

	rows, err := us.UserService.CreateUser(user)
	if err != nil {
		writer.WriteHeader(err.Status)
		if err := json.NewEncoder(writer).Encode(err); err != nil {
			log.Fatalln(err)
		}
		return
	}

	writer.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(writer).Encode(rows); err != nil {
		log.Fatalln(err)
	}
}

func Protected(writer http.ResponseWriter, request *http.Request) {
	if _, err := writer.Write([]byte("Success")); err != nil {
		log.Fatalln(err)
	}
}

func (us *UserController) SignIn(writer http.ResponseWriter, request *http.Request) {
	var user models.User
	if err := json.NewDecoder(request.Body).Decode(&user); err != nil {
		log.Fatalln(err)
	}

	token, err := us.UserService.GetUserByEmail(user)
	if err != nil {
		writer.WriteHeader(err.Status)
		if err := json.NewEncoder(writer).Encode(err); err != nil {
			log.Fatalln(err)
		}
		return
	}

	writer.WriteHeader(http.StatusOK)
	writer.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(writer).Encode(&models.JWT{Token: *token}); err != nil {
		log.Fatalln(err)
	}
}
