package services

import (
	"api/models"
	"api/repositories"
	"api/utils"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

func NewUserService(r repositories.UserRepository) UserService {
	return UserService{Repository: r}
}

type UserService struct {
	Repository repositories.UserRepository
}

func (us *UserService) CreateUser(user models.User) (*models.InsertResult, *models.Error) {
	var e models.Error
	if user.Email == "" || user.Password == "" {
		e.Message = "Email or Password Missing"
		e.Status = http.StatusBadRequest
		return nil, &e
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		log.Fatalln(err)
	}

	user.Password = string(hash)

	result, err := us.Repository.Insert(user)
	if err != nil {
		e.Message = err.Error()
		e.Status = http.StatusInternalServerError
		return nil, &e
	}

	return result, nil
}

func (us *UserService) GetUserByEmail(user models.User) (*string, *models.Error) {
	var e models.Error
	if user.Email == "" || user.Password == "" {
		e.Message = "Email or Password Missing"
		e.Status = http.StatusBadRequest
		return nil, &e
	}

	password := user.Password

	u, err := us.Repository.GetUserByEmail(user.Email)
	if err != nil {
		e.Message = "The user does not exists"
		e.Status = http.StatusBadRequest
		return nil, &e
	}

	hashedPassword := u.Password

	if err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)); err != nil {
		log.Println(err)
		e.Message = "Invalid Password"
		e.Status = http.StatusBadRequest
		return nil, &e
	}

	token, err := utils.GenerateToken(user)
	if err != nil {
		log.Fatalln(err)
	}

	return &token, nil
}
