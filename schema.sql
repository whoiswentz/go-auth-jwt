-- CockroachDB syntax

CREATE DATABASE db_name;
SET database = db_name;

CREATE TABLE IF NOT EXISTS users
(
    id         UUID        NOT NULL DEFAULT gen_random_uuid(),
    name       STRING,
    email      STRING      NOT NULL,
    birth_date TIMESTAMP   NOT NULL,
    document   STRING,
    password   STRING      NOT NULL,
    is_admin   BOOLEAN              DEFAULT FALSE,

    created_at TIMESTAMPTZ NOT NULL DEFAULT now():::TIMESTAMPTZ,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now():::TIMESTAMPTZ,

    FAMILY     "primary"(id, name, email, birth_date, created_at, is_retailer),
    FAMILY     "secondary"(password, is_admin, updated_at),

    CONSTRAINT "primary" PRIMARY KEY (id, email)
);

INSERT INTO users (name, email, birth_date, document, password)
VALUES ($1, $2, $3, $4, $5);

CREATE TABLE IF NOT EXISTS addresses
(
    id           UUID        NOT NULL DEFAULT gen_random_uuid(),
    description  STRING      NOT NULL,
    address_line STRING      NOT NULL,
    number       STRING      NOT NULL,
    suburb       STRING      NOT NULL,
    city         STRING      NOT NULL,
    state        STRING      NOT NULL,
    country      STRING      NOT NULL,
    zip_code     STRING      NOT NULL,

--  Used for maps integration
    lat          STRING,
    lng          STRING,
    alt          STRING,

    created_at   TIMESTAMPTZ NOT NULL DEFAULT now():::TIMESTAMPTZ,
    updated_at   TIMESTAMPTZ NOT NULL DEFAULT now():::TIMESTAMPTZ,

    FAMILY       "primary"(id, country),
    FAMILY       "secondary"(description, address_line, number, suburb, city, state, zip_code, lat, lng, alt),

    CONSTRAINT "primary" PRIMARY KEY (id)
);

CREATE TABLE users_addresses_addresses
(
    usersId     UUID NOT NULL,
    addressesId UUID NOT NULL,
    CONSTRAINT "primary" PRIMARY KEY (usersId, addressesId),
    CONSTRAINT "foreign_key" FOREIGN KEY (addressesId) REFERENCES addresses (id) ON DELETE CASCADE,
    FAMILY      "primary"(usersId, addressesId)
);