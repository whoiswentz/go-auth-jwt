package middlewares

import (
	"api/models"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"os"
	"strings"
)

func TokenVerifyMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")
		bearerToken := strings.Split(authHeader, " ")

		var e models.Error
		if len(bearerToken) == 2 {
			authToken := bearerToken[1]
			token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an error")
				}
				return []byte(os.Getenv("SECRET")), nil
			})

			if err != nil {
				e.Message = err.Error()
				writer.WriteHeader(http.StatusBadRequest)
				if err := json.NewEncoder(writer).Encode(e); err != nil {
					log.Fatalln(err)
				}
			}

			if token != nil && token.Valid {
				next.ServeHTTP(writer, request)
			} else {
				e.Message = err.Error()
				writer.WriteHeader(http.StatusBadRequest)
				if err := json.NewEncoder(writer).Encode(e); err != nil {
					log.Fatalln(err)
				}
			}
		} else {
			e.Message = "Invalid token."
			writer.WriteHeader(http.StatusBadRequest)
			if err := json.NewEncoder(writer).Encode(e); err != nil {
				log.Fatalln(err)
			}
		}
	}
}
